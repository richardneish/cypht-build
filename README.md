# cypht-build

Build and configure [Cypht webmail](https://cypht.org/)

Work-in-progress

# Goal:

Download, build, configure and package the Cypht webapp so the package can be simply copied 
to a webserver without further modification.

# Usage:
 * copy the hm3.ini.dist file to hm3.ini and modify to match your environment.
 * docker build . -t cypht-build
 * docker run -p8080:80 cypht-build
 * Open http://localhost:8080/

# Current status:
Downloads, builds, and configures the Cypht webapp and runs it using the PHP built-in webserver

