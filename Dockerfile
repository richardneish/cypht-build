FROM debian AS builder

# Install build tools
RUN apt-get update -y
RUN apt-get install unzip git composer php-fpm php-curl php-gd php-xml -y

# Clone the repo
WORKDIR /var/www/html
RUN git clone https://github.com/jasonmunro/cypht.git
WORKDIR cypht

# Build the webapp
RUN composer update install

# Copy the config
COPY hm3.ini /usr/local/cypht/

# Run the configuration script
RUN php ./scripts/config_gen.php


############### PHP Extension Installer ###########
FROM mlocati/php-extension-installer AS local-php-extension-installer

################## Runtime container ##############
FROM php:apache

# Install runtime packages
COPY --from=local-php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN install-php-extensions gd curl xml

COPY --from=builder /var/www/html /var/www/html

EXPOSE 80/tcp
